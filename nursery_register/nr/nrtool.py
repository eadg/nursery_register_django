from nr.models import *


class NRtool(object):

    def getmonth(self, mies):
        from calendar import Calendar
        from datetime import date
        d = date.today()
        c = Calendar()
        s = c.itermonthdates(d.year, mies)
        ok = [dej for dej in s if dej.month == mies and dej.weekday() < 5]
        return ok

    def add_kontrahent(self, idko, imi, naz, urod, zlob, gru):
        zlobek = Placowka.objects.all().filter(numer=zlob)[0]
        grup = Grupa.objects.all().filter(nazwa=gru)[0]
        k = Kontrahent(
            idk=idko, nazwisko=naz, imie=imi, urodziny=urod, nr_zlobka=zlobek,
            nr_grupy=grup)
        k.save()
        for mie in range(1, 13):
            for dzie in self.getmonth(mie):
                o = Obecnosc()
                o.dzien = dzie
                o.obecnosc = '?'
                o.kontrahent = k
                o.save()

    def test_getmonth(self, m):
        for i in self.getmonth(m):
            print(i)

# to do: getmonth -> from current month to +3year or better regarding
# date of birth
