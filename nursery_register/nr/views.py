from nr.models import Kontrahent, Obecnosc
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from nr.nrtool import NRtool


def index(request):
    return HttpResponse("Hello WZŻ.")


def first(request, id_www):
    kontrahent = get_object_or_404(Kontrahent, id=id_www)
    return render(request, 'nr/first.html', {'kontrahent': kontrahent})


def time(request):
    from calendar import Calendar
    from datetime import date
    d = date.today()
    c = Calendar()
    s = c.itermonthdates(d.year, 9)
    dejlist = [dej.__str__() for dej in s if dej.month == 9 and dej.weekday() < 5]
    return render(request, 'nr/time.html', {'dejlist': dejlist})


def getgroup(request, id_www):
    dejlist = NRtool().getmonth(9)
    first_day = dejlist[0]
    last_day = dejlist[-1]
    # dejlist = [dej.__str__() for dej in s if dej.month == 9 and dej.weekday() < 5]
    kontrahents = Kontrahent.objects.filter(nr_grupy=id_www).order_by('idk')
    kont_obec_map = {}
    for kontrah in kontrahents:
        obec = Obecnosc.objects.filter(kontrahent__idk=kontrah.idk).filter(
            dzien__gte=first_day).filter(dzien__lte=last_day).order_by('dzien')
        kont_obec_map[kontrah] = obec
    return render(request, 'nr/group.html', {
        'kontrahents': kont_obec_map, 'dejlist': dejlist})

def getattend(request, id_www):
    obec = Obecnosc.objects.filter(obecnosc_id=id_www)[0]
    return render(request, 'nr/editattend.html', {'obec': obec})
