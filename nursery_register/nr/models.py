from django.db import models


class Grupa(models.Model):
    """Nazwy Grup"""
    grupa_id = models.AutoField(primary_key=True)
    nazwa = models.CharField(max_length=5)

    def __str__(self):
        return self.nazwa


class Placowka(models.Model):
    """Adresy"""
    placowka_id = models.AutoField(primary_key=True)
    numer = models.IntegerField(unique=True)
    adres = models.CharField(max_length=200)
    grupa1 = models.CharField(max_length=5, blank=True)
    grupa2 = models.CharField(max_length=5, blank=True)
    grupa3 = models.CharField(max_length=5, blank=True)
    grupa4 = models.CharField(max_length=5, blank=True)
    grupa5 = models.CharField(max_length=5, blank=True)
    grupa6 = models.CharField(max_length=5, blank=True)
    grupa7 = models.CharField(max_length=5, blank=True)
    grupa8 = models.CharField(max_length=5, blank=True)
    grupa9 = models.CharField(max_length=5, blank=True)
    grupa10 = models.CharField(max_length=5, blank=True)
    grupa11 = models.CharField(max_length=5, blank=True)
    grupa12 = models.CharField(max_length=5, blank=True)
    grupa13 = models.CharField(max_length=5, blank=True)
    grupa14 = models.CharField(max_length=5, blank=True)
    grupa15 = models.CharField(max_length=5, blank=True)
    grupa16 = models.CharField(max_length=5, blank=True)
    grupa17 = models.CharField(max_length=5, blank=True)
    grupa18 = models.CharField(max_length=5, blank=True)
    grupa19 = models.CharField(max_length=5, blank=True)
    grupa20 = models.CharField(max_length=5, blank=True)

    def __str__(self):
        return "Zlobek nr {} {}".format(self.numer, self.adres)

    def grupy(self):
        for gr in [
            self.grupa1, self.grupa2, self.grupa3, self.grupa4, self.grupa5,
            self.grupa6, self.grupa7, self.grupa8, self.grupa9, self.grupa10,
            self.grupa11, self.grupa11, self.grupa12, self.grupa13,
            self.grupa14, self.grupa15, self.grupa16, self.grupa17,
                self.grupa18, self.grupa19, self.grupa20]:
            if gr != "nie":
                print(gr)


class Kontrahent(models.Model):
    """Dane podstawowe"""
    kontrahent_id = models.AutoField(primary_key=True)
    idk = models.IntegerField(unique=True)
    nazwisko = models.CharField(max_length=200)
    imie = models.CharField(max_length=200)
    urodziny = models.DateField()
    nr_zlobka = models.ForeignKey(Placowka)
    nr_grupy = models.ForeignKey(Grupa)

    def __str__(self):
        return "Kontrahent {} {} {} ".format(self.idk,
                                             self.nazwisko, self.imie)


class Obecnosc(models.Model):
    """Obecnosc"""
    OBECNY = '1'
    NIEOBECNY = '0'
    ZWOLNIONY = 'X'
    WYPISANY = 'W'
    BRAK = '?'
    OBECNOSC = (
        (OBECNY, 'obecny'),
        (NIEOBECNY, 'nieobecny'),
        (ZWOLNIONY, 'zwolniony'),
        (WYPISANY, 'wypisany'),
        (BRAK, 'brak'),
    )

    obecnosc_id = models.AutoField(primary_key=True)
    kontrahent = models.ForeignKey(Kontrahent)
    dzien = models.DateField()
    obecnosc = models.CharField(max_length=1, choices=OBECNOSC)

    def __str__(self):
        return "{} {} {} ".format(self.kontrahent, self.dzien, self.obecnosc)

    class Meta:
        unique_together = (('kontrahent', 'dzien'),)
