def getmonth(mies):
    from calendar import Calendar
    from datetime import date
    d = date.today()
    c = Calendar()
    s = c.itermonthdates(d.year, mies)
    ok = [dej for dej in s if dej.month == mies and dej.weekday() < 5]
    return ok

print(getmonth(9))
