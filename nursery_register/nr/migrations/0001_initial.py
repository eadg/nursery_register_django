# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Grupa',
            fields=[
                ('grupa_id', models.AutoField(primary_key=True, serialize=False)),
                ('nazwa', models.CharField(max_length=5)),
            ],
        ),
        migrations.CreateModel(
            name='Kontrahent',
            fields=[
                ('kontrahent_id', models.AutoField(primary_key=True, serialize=False)),
                ('idk', models.IntegerField(unique=True)),
                ('nazwisko', models.CharField(max_length=200)),
                ('imie', models.CharField(max_length=200)),
                ('urodziny', models.DateField()),
                ('nr_grupy', models.ForeignKey(to='nr.Grupa')),
            ],
        ),
        migrations.CreateModel(
            name='Obecnosc',
            fields=[
                ('obecnosc_id', models.AutoField(primary_key=True, serialize=False)),
                ('dzien', models.DateField()),
                ('obecnosc', models.CharField(choices=[('1', 'obecny'), ('0', 'nieobecny'), ('X', 'zwolniony'), ('W', 'wypisany'), ('?', 'brak')], max_length=1)),
                ('kontrahent', models.ForeignKey(to='nr.Kontrahent')),
            ],
        ),
        migrations.CreateModel(
            name='Placowka',
            fields=[
                ('placowka_id', models.AutoField(primary_key=True, serialize=False)),
                ('numer', models.IntegerField(unique=True)),
                ('adres', models.CharField(max_length=200)),
                ('grupa1', models.CharField(blank=True, max_length=5)),
                ('grupa2', models.CharField(blank=True, max_length=5)),
                ('grupa3', models.CharField(blank=True, max_length=5)),
                ('grupa4', models.CharField(blank=True, max_length=5)),
                ('grupa5', models.CharField(blank=True, max_length=5)),
                ('grupa6', models.CharField(blank=True, max_length=5)),
                ('grupa7', models.CharField(blank=True, max_length=5)),
                ('grupa8', models.CharField(blank=True, max_length=5)),
                ('grupa9', models.CharField(blank=True, max_length=5)),
                ('grupa10', models.CharField(blank=True, max_length=5)),
                ('grupa11', models.CharField(blank=True, max_length=5)),
                ('grupa12', models.CharField(blank=True, max_length=5)),
                ('grupa13', models.CharField(blank=True, max_length=5)),
                ('grupa14', models.CharField(blank=True, max_length=5)),
                ('grupa15', models.CharField(blank=True, max_length=5)),
                ('grupa16', models.CharField(blank=True, max_length=5)),
                ('grupa17', models.CharField(blank=True, max_length=5)),
                ('grupa18', models.CharField(blank=True, max_length=5)),
                ('grupa19', models.CharField(blank=True, max_length=5)),
                ('grupa20', models.CharField(blank=True, max_length=5)),
            ],
        ),
        migrations.AddField(
            model_name='kontrahent',
            name='nr_zlobka',
            field=models.ForeignKey(to='nr.Placowka'),
        ),
        migrations.AlterUniqueTogether(
            name='obecnosc',
            unique_together=set([('kontrahent', 'dzien')]),
        ),
    ]
