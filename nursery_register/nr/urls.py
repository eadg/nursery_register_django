from django.conf.urls import url, patterns
# from django.conf.urls import patterns, url
from nr import views

# urlpatterns = patterns('', url(r'^$', views.index, name='index'),)
# urlpatterns = ('', url(r'^$', views.index, name='index'),)
urlpatterns = patterns('', url(
    r'^$', views.index, name='index'), url(
    r'^(?P<id_www>\d+)/$', views.first, name='first'), url(
    r'^test/$', views.time, name='time'), url(
    r'^test/grupa/(?P<id_www>\d+)/$', views.getgroup, name='getgroup'), url(
    r'^test/obecnosc/(?P<id_www>\d+)/$', views.getattend, name='getattend'),)
