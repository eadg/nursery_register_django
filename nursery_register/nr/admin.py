from django.contrib import admin
from .models import Kontrahent, Obecnosc, Placowka, Grupa

admin.site.register(Kontrahent)
admin.site.register(Obecnosc)
admin.site.register(Placowka)
admin.site.register(Grupa)
